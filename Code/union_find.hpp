#ifndef UNION_FIND
#define UNION_FIND

#include <iostream>
#include <vector>

using namespace std;

class Union_find {
public:
    Union_find(int lg);
    int racine(int i);
    void ajouter_element(int parent);
    bool fusion(int i, int j);

private:
    vector<int> parents;
};


#endif