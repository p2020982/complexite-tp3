#include "union_find.hpp"

Union_find::Union_find(int lg) {
    parents.clear();
    for(int i = 0; i<lg; ++i) {
        parents.push_back(i);
    }
}


void Union_find::ajouter_element(int parent) {
    if(parents.size() >= parent) {
        parents.push_back(parent);
    }
}

int Union_find::racine(int i) {
    while(i!=parents[i]) {
        i = parents[i];
    }
    return i;
}

bool Union_find::fusion(int i, int j) {
    int rac1 = racine(i);
    int rac2 = racine(j);
    if(rac1 != rac2) {
        parents[rac2] = rac1;
        return true;
    }
    return false;
}

